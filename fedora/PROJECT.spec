Name:             %PROJECT%
Version:          0.0.0
Release:          1
Summary:          %DESCRIPTION%
License:          GPL-3.0-or-later
URL:              https://codeberg.com/ivan.zaera/%PROJECT%

Requires:         nodejs

BuildRequires:    cargo
BuildRequires:    coreutils
BuildRequires:    npm
BuildRequires:    rust
BuildRequires:    scdoc

%description
%DESCRIPTION%

%changelog

%prep
# Preparation is done in Makefile

%build
cd "%{_builddir}/%{name}-%{version}"
make MODE=release build

%check
cd "%{_builddir}/%{name}-%{version}"
make lint
make test

%install
cd "%{_builddir}/%{name}-%{version}"

cp -arv root/* "%{buildroot}"
cp -arv fedora/root/* "%{buildroot}"

mkdir -p "%{buildroot}/usr/bin"
cp -arv rust/target/release/%PROJECT% "%{buildroot}/usr/bin/%PROJECT%"

mkdir -p "%{buildroot}/usr/lib/%PROJECT%/node"
cp -arv node/%PROJECT%/*.js "%{buildroot}/usr/lib/%PROJECT%/node"
cp -ar node/node_modules "%{buildroot}/usr/lib/%PROJECT%/node"

mkdir -p "${buildroot}/usr/share/man/man1"
cp -arv man/build/%PROJECT%.1.gz "%{buildroot}/usr/share/man/man1"

%files

%pre

%post

%preun

%postun

