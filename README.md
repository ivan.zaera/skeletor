# skeletor

A universal project skeleton


## Description

Skeletor is a template repository to bootstrap universal projects. In this
context, _universal_ means that the same project can be used to:

  1. Develop an application locally with parts written in one or more
     programming languages (currently Rust and Node.js are supported).
  2. Perform a local installation with the system's package manager (currently
     only Arch Linux and Fedora are supported).
  3. Release a new version by tagging the source and pushing to Codeberg.
  4. Propagate changes in local installation logic to package manager
     repositories (currently only AUR repositories are supported).


## How to apply Skeletor to a project

Simply choose `skeletor` as your new project template in Codeberg, then clone
the project and run the following command:

```sh
$ make init
```

### How to update the project with new releases

Whenever `skeletor` changes, a new release will be published.

If you want to update your current projects with the new release, clone
`skeletor` locally, next to the projects you want to update and run:

```sh
$ make update PROJECT=path/to/project/to/update
```

That will overwrite the `Makefile`, the scripts in your project's `scripts`
directory and the `arch/release.sh` file, after checking that everything is OK.

You can then review and tweak the changes with `git`, as `skeletor` will refuse
to update if the working copy is not clean (meaning that any changes made to the
files will be due to the update).


## How to use inside the project

All Skeletor actions are driven by the `Makefile`. The current supported targets
are:

  1. clean:      cleans build output directories.
  2. build:      builds artifacts.
  3. format:     formats source files.
  4. lint:       lints source files.
  5. test:       runs tests.
  6. install:    install using the local package manager.
  7. release:    prepare and tag a release.
  8. update-aur: update PKGBUILD file.

### install

The `install` target will make a local installation package and will invoke the OS package manager
to install it.

The local installation package is usually constructed from the current source folder by means of a
production build.

### release

The `release` target offers you a new version number, modifies the package manager files (PKGBUILD
.spec, ...) and then creates a tag and pushes it to the `origin` and `upstream` remotes.

### update-aur

The `update-aur` target receives a `PROJECT` variable that must point to the companion AUR project's
directory. It then updates the `PKGBUILD` in that project based on the contents of the project's
`PKGBUILD`.

You should run this target any time you tweak the project's `PKGBUILD` file.
