#!/bin/bash

####################################################################################################
#
# WARNING: DO NOT EDIT THIS FILE BY HAND AS IT WILL BE OVERWRITTEN BY SKELETOR WHEN YOU RUN UPDATE
#
####################################################################################################

if [[ -d bash ]]; then
	(
		cd bash || exit 1

		if which shellcheck >/dev/null 2>&1; then
			find . -type f -name '*.sh' -exec shellcheck {} \;
		fi
	)
fi

if [[ -d node ]]; then
	(
		cd node || exit 1
		npm install
		npm run lint --workspaces
	)
fi

if [[ -d rust ]]; then
	(
		cd rust || exit 1
		cargo check
	)
fi
