#!/bin/bash

PROJECT_DIR="$(realpath "$(dirname "$0")/..")"
PROJECT_NAME="$(basename "$PROJECT_DIR")"

replace_var() {
	VAR="$1"
	VAL="$2"
	FILE="$3"

	sed -i "s/%${VAR}%/${VAL}/g" "${FILE}"
}

#
# Get configuration data
#
echo -n "Enter project description: "
read -r DESCRIPTION

echo

echo -n "Will you use Bash (Y/n)? "
read -r BASH

echo -n "Will you use man (Y/n)? "
read -r MAN

echo -n "Will you use Node.js (Y/n)? "
read -r NODE

echo -n "Will you use Rust (Y/n)? "
read -r RUST

echo

echo -n "Confirm (y/N)? "
read -r CONFIRM

if [[ "$CONFIRM" != "y" ]]; then
	exit 2
fi

#
# Project
#
CUT_LINE=$(grep -n "# 8< ---- cut here ----" Makefile | cut -d ":" -f 1)
head -$((CUT_LINE - 1)) Makefile >Makefile.cut
mv Makefile.cut Makefile

cat <<EOF >README.md
# ${PROJECT_NAME}

> ${DESCRIPTION}

**${PROJECT_NAME}** does...

## Installation

### Arch Linux

Clone the project and run \`make install\`. It will install **${PROJECT_NAME}** using
\`makepkg\` on this [PKGBUILD](./arch/PKGBUILD).

Alternatively use the [AUR](https://aur.archlinux.org/packages/${PROJECT_NAME}).

### Fedora

Clone the project and run \`make install\`. It will install **${PROJECT_NAME}** using
\`rpmbuild\` on this [spec file](./fedora/${PROJECT_NAME}.spec).

### Any other Linux distro

Feel free to send a pull request with the support needed to make your distro's
package manager install **${PROJECT_NAME}** locally.

You can inspire on Arch Linux installation script to see the steps necessary to
make **${PROJECT_NAME}** work.

### Windows

There are no plans to support **${PROJECT_NAME}** on Windows. Sorry.

## Usage

See \`${PROJECT_NAME}(1)\` man page or its source code in
[scdoc](https://git.sr.ht/~sircmpwn/scdoc) format at
[${PROJECT_NAME}.1.scd](./man/src/${PROJECT_NAME}.1.scd) man page.

## Contributing

Send bugs or pull requests to the
[project's site at Codeberg.org](https://codeberg.org/ivan.zaera/${PROJECT_NAME}).
EOF

rm scripts/init.sh
rm scripts/update.sh

#
# Arch
#
mkdir -p arch/root
replace_var PROJECT "${PROJECT_NAME}" "arch/PKGBUILD"
replace_var PROJECT "${PROJECT_NAME}" "arch/release.sh"
replace_var DESCRIPTION "${DESCRIPTION}" "arch/PKGBUILD"

#
# Bash
#
if [[ "${BASH}" == "n" ]]; then
	rm -rf bash
else
	(
		cd bash || exit 1

		mv PROJECT "${PROJECT_NAME}"
		replace_var PROJECT "${PROJECT_NAME}" "${PROJECT_NAME}"
	)
fi

#
# Fedora
#
mkdir -p fedora/root
mv "fedora/PROJECT.spec" "fedora/${PROJECT_NAME}.spec"
replace_var PROJECT "${PROJECT_NAME}" "fedora/${PROJECT_NAME}.spec"
replace_var DESCRIPTION "${DESCRIPTION}" "fedora/${PROJECT_NAME}.spec"

#
# Linux
#
mkdir -p root

#
# Man
#
if [[ "${MAN}" == "n" ]]; then
	rm -rf man
else
	(
		cd man/src || exit 1

		mv PROJECT.1.scd "${PROJECT_NAME}.1.scd"
		replace_var PROJECT "${PROJECT_NAME}" "${PROJECT_NAME}.1.scd"
		replace_var DESCRIPTION "${DESCRIPTION}" "${PROJECT_NAME}.1.scd"
	)
fi

#
# Node
#
if [[ "${NODE}" == "n" ]]; then
	rm -rf node
else
	(
		cd node || exit 1

		replace_var PROJECT "${PROJECT_NAME}" "package.json"

		mkdir "${PROJECT_NAME}"
		cd "${PROJECT_NAME}" || exit 1
		npm init -y
	)
fi

#
# Rust
#
if [[ "${RUST}" == "n" ]]; then
	rm -rf rust
else
	(
		cd rust || exit 1

		replace_var PROJECT "${PROJECT_NAME}" "Cargo.toml"

		cargo new "${PROJECT_NAME}"
	)
fi
