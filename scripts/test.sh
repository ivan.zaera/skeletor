#!/bin/bash

####################################################################################################
#
# WARNING: DO NOT EDIT THIS FILE BY HAND AS IT WILL BE OVERWRITTEN BY SKELETOR WHEN YOU RUN UPDATE
#
####################################################################################################

if [[ -d node ]]; then
	(
		cd node || exit 1
		npm install
		npm run test --workspaces
	)
fi

if [[ -d rust ]]; then
	(
		cd rust || exit 1
		cargo test
	)
fi
