"
" The contract of this file is that it will be sourced whenever a buffer
" belonging to the project containing this file is entered.
"
" The previous chapter means you can switch between projects in the same
" Neovim session if you open files from different project trees, leading to
" re-sourcings of this file.
"
" Per the above reasons this file should be idempotent in respect to sourcing
" and should normally be used to just set project makers, linters, etc.
"
" Don't add resource intensive tasks for this file because it can slow down
" opening files.
"


:MyProjectSet name skeletor(scripts)
:MyProjectSet compiler my-shellcheck
:MyProjectSet formatter shfmt
