#!/bin/bash

SKELETOR_DIR="$(realpath "$(dirname "$0")/..")"

replace_var() {
	VAR="$1"
	VAL="$2"
	FILE="$3"

	sed -i "s/%${VAR}%/${VAL}/g" "${FILE}"
}

#
# Check status
#
if [[ "${PROJECT}" == "" ]]; then
	echo
	echo ERROR: PROJECT variable must be provided
	echo
	exit 1
fi

if [[ ! -r "${PROJECT}/Makefile" ]]; then
	echo
	echo ERROR: "${PROJECT}"/Makefile does not exist
	echo
	exit 1
fi

cd "${PROJECT}" || exit 1
if [ -n "$(git status --porcelain)" ]; then
	echo
	echo ERROR: "${PROJECT}" working copy is not clean
	echo
	exit 1
fi
cd "${SKELETOR_DIR}" || exit 1

#
# Copy things
#
cp Makefile "${PROJECT}/Makefile"
cp scripts/* "${PROJECT}/scripts"
cp arch/release.sh "${PROJECT}/arch/release.sh"

#
# Tweak things
#
cd "${PROJECT}" || exit 1

PROJECT_NAME=$(basename "$PWD")

CUT_LINE=$(grep -n "# 8< ---- cut here ----" Makefile | cut -d ":" -f 1)
head -$((CUT_LINE - 1)) Makefile >Makefile.cut
mv Makefile.cut Makefile

rm scripts/init.sh
rm scripts/update.sh

replace_var PROJECT "${PROJECT_NAME}" "arch/release.sh"
